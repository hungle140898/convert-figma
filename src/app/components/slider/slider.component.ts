import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {
  constructor() {}
  ngOnInit(): void {}
  public dataImg = [
    '../../../assets/images/banner-shop.png',
    '../../../assets/images/banner2.png',
    '../../../assets/images/image 59.png',
    '../../../assets/images/banner-shop.png',
    '../../../assets/images/banner-shop.png',
  ];
}
