import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public show: boolean = false;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  onShow() {
    this.show = !this.show;
  }
  route() {
    this.router.navigateByUrl('#').then();
  }
}
